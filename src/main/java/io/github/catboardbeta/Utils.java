/*
 * Syml, open source scripting language.
 * Copyright (c) 2021 William Nelson
 *
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This software is licensed under the MIT License. View LICENSE 
 * for more information.
 */

package io.github.catboardbeta;

public class Utils {

    private Utils() {
        // THY SHALLN'T HAVE CONSTRUCTOR
    }

    public static Character[] charToCharacterArray(char[] arr) {
        Character[] ret = new Character[arr.length];

        for (int i = 0; i < arr.length; i++) {
            ret[i] = arr[i];
        }

        return ret;
    }
}
