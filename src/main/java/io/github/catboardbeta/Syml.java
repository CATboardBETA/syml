/*
 * Syml, open source scripting language.
 * Copyright (c) 2021 William Nelson
 *
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This software is licensed under the MIT License. View LICENSE 
 * for more information.
 */

package io.github.catboardbeta;

import java.io.File;
import java.io.IOException;

public final class Syml {

    public static final String VERSION = "v1.0.0";

    private static final String usage = "Usage:\n" +
            "syml <input.sml>";

    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            printUsage();
            System.exit(-1);
        }

        File input = new File(args[0]);
        ScanFile.scan(input);
    }

    private static void printUsage() {
        System.out.println(usage);
    }
}
