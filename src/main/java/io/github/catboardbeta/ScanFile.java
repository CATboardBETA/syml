/*
 * Syml, open source scripting language.
 * Copyright (c) 2021 William Nelson
 *
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This software is licensed under the MIT License. View LICENSE 
 * for more information.
 */

package io.github.catboardbeta;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static io.github.catboardbeta.Tokens.*;

public class ScanFile {

    private ScanFile() {
        // This class does not have a constructor
    }

    public static void scan(File input) throws IOException {
        System.out.println("Running checks on file...");
        checkFileUsability(input);
        System.out.println("Checks passed...");


        BufferedReader br = null;
        try {
            System.out.println("Processing input file...");
            br = new BufferedReader
                    (new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            System.out.println("FATAL: The input file was deleted while processing");
            System.exit(1);
        }

        StringBuilder sb = new StringBuilder();
        while(br.ready()) {
            sb.append(br.readLine());
            sb.append('\n');
        }
        br.close();

        System.out.println("Parsing file to tokens...");
        Token[] tkList = tokenize(sb.toString());
        System.out.println("Finished parsing to tokens...");
        System.out.println("Running token validity checks...");
        checkTokenValidity(tkList);
        System.out.println("Token validity checks all passed...");
        System.out.println("Executing...");
        executeTokens(tkList);
    }

    private static void executeTokens(Token[] tokens) {
        System.out.println(Arrays.toString(tokens));
        for (int i = 0; i < tokens.length; i++) {
            Token token = tokens[i];

            switch (token.token()) {
                case BANG -> {}
                case BANG_EQUALS -> {}
                case EQUALS -> {}
                case EQUALS_EQUALS -> {}
                case MINUS -> {}
                case NEXT_TOKEN -> {}
                case NUM_LITERAL -> {
                    System.out.println("FATAL: NEXT_TOKEN can only be a token content, not standalone.");
                    System.exit(-1);
                    }
                case PLUS -> {}
                case PREVIOUS_TOKEN -> {
                    System.out.println("FATAL: PREVIOUS_TOKEN can only be a token content, not standalone.");
                    System.exit(-1);
                }
                case SEMICOLON -> {
                    // Not yet implemented.
                }
                case SLASH -> {}
                case STAR -> {}
                case STR_LITERAL -> {
                    System.out.println(token.content().get(0));
                }
            }
        }
    }

    private static void checkTokenValidity(Token[] tokens) {
        
        System.out.println("Checking for empty...");
        if (List.of(tokens).isEmpty()) {
            System.out.println("FATAL: No tokens present. Is the file empty?");
        }
        System.out.println("Check passed...");
        
        Token firstToken = tokens[0];
        Token lastToken = tokens[tokens.length - 1];

        // last token cannot contain NEXT_TOKEN
        if ((lastToken.content() != null ? lastToken.content() : new ArrayList<>()
                ).contains(NEXT_TOKEN)) {
            System.out.println("FATAL: Last token cannot reference the next token as it doesn't exist.");
            System.exit(4);
        }

        // first token cannot contain PREVIOUS_TOKEN
        System.out.println("Checking for first token referencing previous...");
        if((firstToken.content() != null ? firstToken.content() : new ArrayList<>()
                ).contains(PREVIOUS_TOKEN)) {
            System.out.println("FATAL: First token cannot reference the previous token as it doesn't exist.");
            System.exit(4);
        }
        System.out.println("Check passed...");

        boolean containsPreviousToken = false;
        boolean containsNextToken     = false;
        for (int i = 0; i < tokens.length; i++) {

            Token tk = tokens[i];

            // Set if previous token is in content
            if ((tk.content() != null ? tk.content() : new ArrayList<>()
                    ).contains(PREVIOUS_TOKEN)) {
                containsPreviousToken = true;
            } else {
                containsPreviousToken = false;
            }

            // Set if next token is in content
            if ((tk.content() != null ? tk.content() : new ArrayList<>()
                    ).contains(NEXT_TOKEN)) {
                containsNextToken = true;
            } else {
                containsNextToken = false;
            }

            try {
                if (containsPreviousToken && tokens[i - 1].token() != NUM_LITERAL) {
                    System.out.println("FATAL: token " 
                        + tk.token() 
                        + " not applicable to PREVIOUS_TOKEN = " 
                        + tokens[i - 1].token());
                    System.exit(5);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                // We can ignore this.
            }
        }
    }

    private static Token[] tokenize(String fcontents) {
        ArrayList<Token> tokenList = new ArrayList<>();

        Character[] contentsAsArray = Utils.charToCharacterArray(fcontents.toCharArray());
        Character current;
        Character next;
        for (int i = 0; i < contentsAsArray.length; i++) {
            current = contentsAsArray[i];

            try { next = getNext(contentsAsArray, i);} catch(IndexOutOfBoundsException e) {
                // Nothing to do. It will simply not run another loop.
                next = null;
            }

            ArrayList<Object> contents = new ArrayList<>();
            switch (current) {
                case '!' -> {
                    if (Objects.equals(next, '=')) {
                        contents.add(PREVIOUS_TOKEN);
                        contents.add(NEXT_TOKEN);
                        tokenList.add(new Token(BANG_EQUALS, contents));
                        // The next iteration should skip the equals, as it's token has already been added
                        i++;
                    } else {
                        contents.add(NEXT_TOKEN);
                        tokenList.add(new Token(BANG, contents));
                    }
                }
                case '=' -> {
                    if (Objects.equals(next, '=')) {
                        contents.add(PREVIOUS_TOKEN);
                        contents.add(NEXT_TOKEN);
                        tokenList.add(new Token(EQUALS_EQUALS, contents));
                        // Similarly, it should skip the second equals
                        i++;
                    } else {
                        contents.add(PREVIOUS_TOKEN);
                        contents.add(NEXT_TOKEN);
                        tokenList.add(new Token(EQUALS, contents));
                    }
                }
                case ';' -> {
                    tokenList.add(new Token(SEMICOLON, contents));
                    i++;
                }
                case '-' -> {
                    contents.add(PREVIOUS_TOKEN);
                    contents.add(NEXT_TOKEN);
                    tokenList.add(new Token(MINUS, contents));
                    i++;
                }
                case '+' -> {
                    contents.add(PREVIOUS_TOKEN);
                    contents.add(NEXT_TOKEN);
                    tokenList.add(new Token(PLUS, contents));
                    i++;
                }
                case '*' -> {
                    contents.add(PREVIOUS_TOKEN);
                    contents.add(NEXT_TOKEN);
                    tokenList.add(new Token(STAR, contents));
                    i++;
                }
                case '/' -> {
                    contents.add(PREVIOUS_TOKEN);
                    contents.add(NEXT_TOKEN);
                    tokenList.add(new Token(SLASH, contents));
                    i++;
                }
                case '"' -> {
                    StringBuilder sb = new StringBuilder();
                    for (int j = i + 1; contentsAsArray.length > j && contentsAsArray[j] != '"'; j++) {
                        sb.append(contentsAsArray[j]);
                        i++;
                    }
                    contents.add(sb.toString());
                    tokenList.add(new Token(STR_LITERAL, contents));
                    i++;
                }
                case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' -> {

                    StringBuilder sb = new StringBuilder();
                    sb.append(contentsAsArray[i]);
                    for (int j = i + 1; contentsAsArray.length > j && isNumeral(contentsAsArray[j]); j++) {
                        sb.append(contentsAsArray[j]);
                        i++;
                    }
                    contents.add("0" + sb);
                    tokenList.add(new Token(NUM_LITERAL, contents));
                    i++;
                }
                default -> {
                    if (!Character.isWhitespace(contentsAsArray[i])) {
                        System.out.println("WARN: Character '" + contentsAsArray[i] + "' is not recognized. Ignored. (Char " + i + " including whitespace");
                    }
                }
            }
        }

        for (Token o : tokenList) {
            Tokens tk = o.token();
            String content = o.content() != null ? o.content().toString() : "";
            System.out.println("Token:" + tk + " || Content:" + content);
        }

        return tokenList.toArray(new Token[0]);
    }

    private static boolean isNumeral(Character c) {
        return c == '0'
                || c == '1'
                || c == '2'
                || c == '3'
                || c == '4'
                || c == '5'
                || c == '6'
                || c == '7'
                || c == '8'
                || c == '9'
                || c == '.';
    }

    private static Character getNext(@NotNull Character[] contentsAsArray, int index) {
        return contentsAsArray[index + 1];
    }

    /**
     * Check if the input file is scan-able. It
     * must be readable and exist.
     * @param input file to check
     */
    private static void checkFileUsability(@NotNull File input) {
        if (!input.exists()) {
            System.out.println("FATAL: Input file does not exist");
            System.exit(1);
        } else if (input.isDirectory()) {
            System.out.println("FATAL: Input file is a directory");
            System.exit(2);
        } else if (!input.canRead()) {
            System.out.println("FATAL: Input file is unreadable");
            System.exit(3);
        }
    }
}
