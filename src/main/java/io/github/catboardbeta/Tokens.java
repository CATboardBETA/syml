/*
 * Syml, open source scripting language.
 * Copyright (c) 2021 William Nelson
 *
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This software is licensed under the MIT License. View LICENSE 
 * for more information.
 */

package io.github.catboardbeta;

public enum Tokens {
    BANG, BANG_EQUALS, EQUALS, EQUALS_EQUALS, MINUS, NEXT_TOKEN, NUM_LITERAL, PLUS, PREVIOUS_TOKEN, SEMICOLON, SLASH, STAR, STR_LITERAL

}
