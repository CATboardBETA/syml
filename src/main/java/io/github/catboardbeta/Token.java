/*
 * Syml, open source scripting language.
 * Copyright (c) 2021 William Nelson
 *
 *
 * Syml is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This software is licensed under the MIT License. View LICENSE 
 * for more information.
 */

package io.github.catboardbeta;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public record Token(@NotNull Tokens token, @Nullable ArrayList<Object> content) {

    public Token(@NotNull Tokens token) {
        this(token, null);
    }
}
